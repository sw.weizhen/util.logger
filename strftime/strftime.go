package strftime

import (
	"io"
	"sync"
	"time"
)

type strftimeHandler interface {
	handle(Appender)
}

type appenderListBuilder struct {
	apdList *mergingAppend
}

func (ref *appenderListBuilder) handle(apd Appender) {
	ref.apdList.Append(apd)
}

type appenderExec struct {
	t   time.Time
	dst []byte
}

func (ref *appenderExec) handle(apd Appender) {
	ref.dst = apd.Append(ref.dst, ref.t)
}

var fmtApdExecPool = sync.Pool{
	New: func() interface{} {
		var h appenderExec
		h.dst = make([]byte, 0, 32)
		return &h
	},
}

type StringFormatTime struct {
	pattern      string
	anthologized appenderList
}

func (ref *StringFormatTime) format(bt []byte, t time.Time) []byte {
	for _, apdL := range ref.anthologized {
		bt = apdL.Append(bt, t)
	}

	return bt
}

func (ref *StringFormatTime) Pattern() string {
	return ref.pattern
}

func (ref *StringFormatTime) FormatString(t time.Time) string {
	const bufSize = 64
	var bt []byte
	max := len(ref.pattern) + 10
	if max < bufSize {
		var buf [bufSize]byte
		bt = buf[:0]

	} else {
		bt = make([]byte, 0, max)

	}

	return string(ref.format(bt, t))
}

func (ref *StringFormatTime) Dump(out io.Writer) {
	ref.anthologized.dump(out)
}
