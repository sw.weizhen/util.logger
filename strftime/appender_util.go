package strftime

import (
	"strconv"
	"strings"
	"time"
)

func StdlFormat(str string) Appender {
	return &stdlFormat{str: str}
}

func Verbatimer(str string) Appender {
	return &verbatimer{str: str}
}

func Milliseconds() Appender {
	return milliseconds
}

func Microseconds() Appender {
	return microseconds
}

func UnixSeconds() Appender {
	return unixseconds
}

func mergeable(str string) bool {
	if strings.ContainsAny(str, "0987654321") {
		return false
	}

	for _, verb := range keywordExclusion {
		if strings.Contains(str, verb) {
			return false
		}
	}

	return true
}

func appendDayOfYear(bt []byte, t time.Time) []byte {
	n := t.YearDay()
	if n < 10 {
		bt = append(bt, '0', '0')
	} else if n < 100 {
		bt = append(bt, '0')
	}
	return append(bt, strconv.Itoa(n)...)
}
