package rotatelogs

import (
	"log"

	lgg "gitlab.com/sw.weizhen/util.logger/rotatefiles"
)

type RotateLog struct {
	rtlg       *log.Logger
	fileOutput *lgg.RotateLogs

	mode   int
	system int
}
