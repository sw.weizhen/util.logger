package rotatefiles

import (
	"sync"
)

type cleaner struct {
	enable bool
	fn     func()
	mutex  sync.Mutex
}

func (ref *cleaner) Enable() {
	ref.mutex.Lock()
	defer ref.mutex.Unlock()
	ref.enable = true
}

func (ref *cleaner) Run() {
	ref.fn()
}
