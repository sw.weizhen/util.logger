package main

import (
	"fmt"

	lgg "gitlab.com/sw.weizhen/util.logger"
)

var path string = "log/"
var size int = 50

func main() {
	fmt.Println("main::main()")

	ftl, err := lgg.New(lgg.SYS_WINDOWS, lgg.MODE_DEBUG, size, path)
	if err != nil {
		fmt.Printf("logger error: %v\n", err)
		return
	}

	ftl.Info("test")
}
